#!/bin/bash

apt-get update
apt-get install -y openjdk-17-jdk git maven

git clone https://github.com/spring-projects/spring-petclinic.git /home/vagrant/petclinic

cd /home/vagrant/petclinic
./mvnw package

mv target/*.jar /home/vagrant/app.jar

chown -R vagrant:vagrant /home/vagrant

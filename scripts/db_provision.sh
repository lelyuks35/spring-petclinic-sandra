#!/bin/bash

apt-get update
apt-get install -y mysql-server

sed -i "s/bind-address\s*=.*/bind-address = 192.168.56.10/" /etc/mysql/mysql.conf.d/mysqld.cnf
systemctl restart mysql

mysql -u root <<-EOF
  CREATE DATABASE petclinic;
  CREATE USER 'dbuser'@'192.168.56.%' IDENTIFIED BY 'dbpassword';
  GRANT ALL PRIVILEGES ON petclinic.* TO 'dbuser'@'192.168.56.%';
  FLUSH PRIVILEGES;
EOF
